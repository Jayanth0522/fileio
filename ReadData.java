package com.innominds.files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
public class ReadData {
	@SuppressWarnings("unchecked")
	public void updateData() throws IOException {
		File file=new File("C:\\\\Users\\\\jpalepally\\\\eclipse-workspace\\\\SocialNetwork\\\\src\\\\jsonfile.json");
		FileWriter filewriter=new FileWriter (file);
		JSONObject firstuser = new JSONObject();
    	firstuser .put("firstName", "Jayanth");
    	firstuser .put("age", "23");
    	firstuser .put("designation", "IT employee");
    	firstuser .put("location", "Hyderabad");
    	JSONArray listofhobbies=new JSONArray();
    	listofhobbies.add("Badminton");
    	listofhobbies.add("Guitarist");
    	firstuser.put("hobbies",listofhobbies);
     
    	JSONObject userObject = new JSONObject(); 
    	userObject.put("user", firstuser );
     
    	//Second user
    	JSONObject seconduser = new JSONObject();
    	seconduser.put("firstName", "Ruchi");
    	seconduser.put("age", "22");
    	seconduser.put("designation", "system engineer");
    	JSONArray listofhobbies2=new JSONArray();
    	listofhobbies2.add("chatting");
    	listofhobbies2.add("Listening Music");
    	seconduser.put("hobbies",listofhobbies2);
     
    	JSONObject userObject2 = new JSONObject(); 
    	userObject2 .put("user", seconduser);
     
    	//Add users to list
    	JSONArray userList = new JSONArray();
    	userList.add(userObject);
    	userList.add(userObject2);
     
    	//Write JSON file
    	filewriter.write( userList.toJSONString()); 
    	filewriter.flush();
    	filewriter.close();
    	}
	}


